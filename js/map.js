canvas.addEventListener('mousedown', down)
canvas.addEventListener('touchstart', down)

const radius = 30
let prevX, prevY

function down(e) {
    const rect = e.target.getBoundingClientRect()
    prevX = (e.touches ? e.touches[0].clientX : e.clientX) - rect.left
    prevY = (e.touches ? e.touches[0].clientY : e.clientY) - rect.top
    canvas.addEventListener('mousemove', move)
    canvas.addEventListener('touchmove', move)
    document.addEventListener('mouseup', up)
    document.addEventListener('touchend', up)
}

function move(e) {
    const rect = e.target.getBoundingClientRect()

    const dx = rect.left + prevX - (e.touches ? e.touches[0].clientX : e.clientX) 
    const dy = rect.top + prevY - (e.touches ? e.touches[0].clientY : e.clientY)

    prevX = (e.touches ? e.touches[0].clientX : e.clientX) - rect.left 
    prevY = (e.touches ? e.touches[0].clientY : e.clientY) - rect.top

    const r = 128 + Math.max(-128, Math.min(dx * 2, 128))
    const g = 128 + Math.max(-128, Math.min(dy * 2, 128))
    const b = 128

    contextMap.rect(prevX - radius, prevY - radius, radius * 2, radius * 2)

    const grad = contextMap.createRadialGradient(prevX, prevY, 0, prevX, prevY, radius)
    grad.addColorStop(0, `rgba(${r}, ${g}, ${b}, 1)`)
    grad.addColorStop(1, `rgba(${r}, ${g}, ${b}, 0)`)

    contextMap.fillStyle = grad
    contextMap.fill()
}

function up() {
    canvas.removeEventListener('mousemove', move)
    canvas.removeEventListener('touchmove', move)
    document.removeEventListener('mouseup', up)
    document.removeEventListener('touchend', up)
}
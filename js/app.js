const video = document.getElementById('webcam')
let device = { facingMode: 'user' }

navigator.mediaDevices.enumerateDevices().then(devices => {
    devices = devices.filter(d => d.kind === 'videoinput')
    device = devices.length ? { exact: devices[0].deviceId } : device
    navigator.mediaDevices.getUserMedia({
        video: device,
        audio: false,
    })
        .then(function (stream) {
            video.srcObject = stream
        })
        .catch(console.error)    
})

video.addEventListener('canplay', start)

const source = document.getElementById('source')
const map = document.getElementById('map')
const canvas = document.getElementById('canvas')

const contextSrc = source.getContext('2d', { willReadFrequently: true })
contextSrc.translate(source.width, 0)
contextSrc.scale(-1, 1)

const contextMap = map.getContext('2d', { willReadFrequently: true })

function start() {
    if (video.videoWidth < video.videoHeight) {
        video.width = source.width = canvas.width = map.width = '240'
        video.height = source.height = canvas.height = map.height = '320'
    }
    
    contextMap.fillStyle = '#808080'
    contextMap.fillRect(0, 0, source.width, source.height)
    
    filter = new filters.DisplacementMap(source, map, canvas, new filters.Point(), 25, 25, filters.ColorChannel.RED, filters.ColorChannel.GREEN)
    draw()
}

screen.orientation.addEventListener('change', () => {
    const w = video.videoWidth < video.videoHeight ? '240' : '320'
    const h = w === '320' ? '240' : '320'
    video.width = source.width = canvas.width = map.width = w
    video.height = source.height = canvas.height = map.height = h
    contextMap.fillStyle = '#808080'
    contextMap.fillRect(0, 0, source.width, source.height)
});

function draw() {
    window.requestAnimationFrame(draw)
    contextSrc.drawImage(video, 0, 0, video.width, video.height)
    filter.draw()
}
